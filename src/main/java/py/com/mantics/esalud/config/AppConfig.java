package py.com.mantics.esalud.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import py.com.mantics.base.config.BaseWebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan({ "py.com.mantics" })
@Import(AppSecurityConfig.class)
public class AppConfig extends BaseWebMvcConfigurerAdapter {

}
