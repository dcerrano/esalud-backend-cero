CREATE USER esalud PASSWORD 'esaludpass' SUPERUSER;
CREATE SCHEMA audit;

INSERT INTO permiso(id, codigo, descrip) VALUES (1, 'root','Permiso para ser Super Usuario');
INSERT INTO rol(id, codigo, descrip, groupid) VALUES(1, 'root','Rol SuperUsuario', 1);
INSERT INTO rolpermiso(id, permiso_id, rol_id) VALUES(1, 1, 1);
INSERT INTO usuario(id, username, nombres, apellidos, activo, email, password, genero, rol_id) VALUES (1, 'admin', 'Super', 'Usuario', True, 'root@gmail.com', MD5('admin'), 'M', 1);

